var allArticles = document.querySelectorAll('article');
var oneArticles = document.querySelectorAll('#one article');
var threeArticles = document.querySelectorAll('#three article');
var boxes = document.querySelectorAll('.box');
var secondMore = document.querySelector('#second-more-button');
var secondMoreId = document.getElementById('second-more-button');
var socialLinksUl = document.getElementsByClassName('icons');
var socialLinks = document.getElementsByClassName('icon');
var copyright = document.querySelector('#footer .copyright');
var bannerH1 = document.querySelector('#banner h1');


// console.log(boxes.style); //wont work because boxes is an array

boxes.forEach(function(box, key){

	// console.log(box.style); // check to see if style object is available

	box.style.border = 'solid 1px rgba(255, 0, 255, .5)';

	// console.log(box.querySelector('h3')); // test if you can use querySelect on box

	box.querySelector('h3').style.color = "#b60001";

});

console.log(socialLinks[0]);

for (var i = 0; i < socialLinks.length; i++) {

	// var icon[i] = socialLinks[i];

	socialLinks[i].addEventListener('mouseover', function(event){
		// this.style.color = "#b60001";
		// console.log(this.classList.add('hover-color'));
		this.classList.add('hover-color');
	});

	socialLinks[i].addEventListener('mouseout', function(event){
		// this.style.color = "#5385c1";
		this.classList.remove('hover-color');
	});

	// console.log(i);
}


// socialLinks.forEach(function(icon, key){

// 	icon.style.color = "#b60001";

// });

form = document.getElementById('myForm');

form.addEventListener('submit', function(event){
	event.preventDefault();
	
	console.log(event);
});


// boxes.style